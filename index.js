const express = require("express");
const mongoose = require("mongoose");
const userRoute = require("./routers/userrouter");
const albumRoute = require("./routers/albumrouter");
var uri = "mongodb://localhost:27017/user_app";
var fs = require("fs");

if (!fs.existsSync("./storage/")) {
  fs.mkdirSync("./storage");
  console.log("crreated storage folder");
}
if (!fs.existsSync("./storage/profile/")) {
  fs.mkdirSync("./storage/profile");
  console.log("crreated profile folder");
}

if (!fs.existsSync("./storage/album/")) {
  fs.mkdirSync("./storage/album");
  console.log("crreated album folder");
}

mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});
const connection = mongoose.connection;
connection.once("open", function () {
  console.log("MongoDB database connection established successfully");
});

const app = express();
const port = process.env.PORT || 4000;
app.use("/storage", express.static("storage"));

app.use(express.json());
app.use(userRoute);
app.use(albumRoute);

app.get("*", async (req, res) => {
  res.status(404).send("Invalid request");
});

app.listen(port, () => {
  console.log("Server is up on port " + port);
});
