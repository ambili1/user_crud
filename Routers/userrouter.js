const express = require("express");
const upload = require("../middleware/upload");
const router = express.Router();
const auth = require("../middleware/auth");

var user_controller = require("../controller/usercontroler");

router.get("/", user_controller.index);

router.post("/create", user_controller.create_user);

router.patch("/user/edit", auth, user_controller.edit_user);

router.get("/user/view", auth, user_controller.view_profile);

router.post("/user/pic", auth, upload.single("pic"), user_controller.uplod_pic);

router.delete("/user/delete", auth, user_controller.delete_user);

router.post("/login", user_controller.login);

router.post("/logout", auth, user_controller.logout);

router.get("/user/list", auth, user_controller.list_user);

module.exports = router;
