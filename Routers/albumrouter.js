const express = require("express");
const albumupload = require("../middleware/albumupload");
const router = express.Router();
const auth = require("../middleware/auth");
const albumauth = require("../middleware/albumauth");
var album_controller = require("../controller/albumcontroler");

router.post("/album/create", auth, album_controller.create_album);

router.patch("/album/edit/:id", auth, album_controller.edit_album);

router.get("/album/view/:id", auth, albumauth, album_controller.view_album);

router.get("/album/list", auth, album_controller.list_album);

router.post(
  "/album/:id/addimage",
  auth,
  albumauth,
  albumupload.single("image"),
  album_controller.add_albumimage
);

router.delete(
  "/album/:id/deleteimage/:img_id",
  auth,
  albumauth,
  album_controller.delete_image
);

router.delete(
  "/album/delete/:id",
  auth,
  albumauth,
  album_controller.delete_album
);

module.exports = router;
