const jwt = require("jsonwebtoken");
const User = require("../models/User");
const Album = require("../models/Album");
const Albumimages = require("../models/Albumimages");
var fs = require("fs");
const JWT_SECRET = "ambily7559711";
const bcrypt = require("bcryptjs");

exports.index = async function (req, res) {
  console.log(res.connection.localAddress);
};

exports.create_user = async function (req, res) {
  try {
    await User.find({ email: req.body.email }, async function (err, check) {
      if (check[0]) {
        return res
          .status(400)
          .send({ statusCode: 400, message: "This user  email alredy exist" });
      } else {
        obj = req.body;
        obj.dob = new Date(req.body.dob);
        obj.password = await bcrypt.hash(req.body.password, 8);
        const user = new User(obj);

        await user.save();
        const token = jwt.sign({ _id: user._id.toString() }, JWT_SECRET);

        user.tokens = user.tokens.concat({ token });
        await user.save();

        res.status(201).send({ statusCode: 201, user, token });
      }
    });
  } catch (e) {
    res.status(400).send({ statusCode: 400, error: e });
  }
};

exports.edit_user = async function (req, res) {
  const updates = Object.keys(req.body);
  const allowedUpdates = [
    "name",
    "email",
    "phone",
    "password",
    "dob",
    "address",
  ];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ statusCode: 400, error: "Invalid updates!" });
  }

  try {
    updates.forEach((update) => (req.user[update] = req.body[update]));
    if (req.body.password !== undefined) {
      req.user.password = await bcrypt.hash(req.body.password, 8);
    }
    await req.user.save();
    res.send({ statusCode: 200, message: "user updated" });
  } catch (e) {
    res.status(400).send({ statusCode: 400, error: e });
  }
};

/*
 */

exports.uplod_pic = function (req, res) {
  function fileup() {
    req.user.pic = {
      name: req.file.filename,
    };

    req.user.save();
    res.status(200).send({ statusCode: 200, message: "upload sucessful" });
  }

  try {
    const path = "./storage/profile/" + req.user.pic.name + "";

    fs.access(path, fs.F_OK, (err) => {
      if (err) {
        fileup();
      } else {
        fs.unlinkSync(path);
        fileup();
      }
    });
  } catch (error) {
    res.status(400).send({ statusCode: 400, error: error.message });
  }
};

exports.delete_user = async function (req, res) {
  try {
    if (req.user.pic.name !== undefined) {
      var path = "./storage/profile" + req.user.pic.name + "";
      fs.access(path, fs.F_OK, (err) => {
        if (!err) {
          fs.unlinkSync(path);
        }
      });
    }

    var album = await Album.find({ owner: req.user._id });
    if (album) {
      for (let i = 0; i < album.length; i++) {
        var img = await Albumimages.find({ owner: album[i]._id });

        for (let j = 0; j < img.length; j++) {
          if (img[j]) {
            var path = "./storage/album/" + img[j].image + "";
            fs.access(path, fs.F_OK, (err) => {
              if (!err) {
                fs.unlinkSync(path);
              }
            });
            await Albumimages.findOneAndDelete({ _id: img[j]._id });
          }
        }
        await Album.findOneAndDelete({ _id: album[i]._id });
      }
    }

    await req.user.remove();
    res.send({ statusCode: 200, meaasge: "your account deleted" });
  } catch (error) {
    res.status(400).send({ statusCode: 400, error: error.message });
  }
};

exports.login = async function (req, res) {
  var email = req.body.email;
  try {
    var user = await User.findOne({ email });

    if (!user) {
      return res.status(500).send({
        statusCode: 500,
        message: "Unable to login, User dosent exist",
      });
    }

    const isMatch = await bcrypt.compare(req.body.password, user.password);

    if (!isMatch) {
      return res.status(500).send({
        statusCode: 500,
        message: "Unable to login,Password Incorrect",
      });
    }
    const token = jwt.sign({ _id: user._id.toString() }, JWT_SECRET);

    user.tokens = user.tokens.concat({ token });
    await user.save();
    res.status(200).send({ statusCode: 200, user, token });
  } catch (e) {
    res.status(500).send({ statusCode: 500, error: e });
  }
};

exports.logout = async function (req, res) {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.user.save();

    res.status(200).send({ statusCode: 200, message: "logout succesfuly" });
  } catch (e) {
    res.status(500).send({ statusCode: 500, error: e });
  }
};

exports.view_profile = function (req, res) {
  var obj = req.user;
  var p = res.connection.localPort;
  if (obj.pic.name !== undefined) {
    obj.pic.path =
      "http://localhost:" + p + "/storage/profile/" + obj.pic.name + "";
  }
  res.status(200).send({ statusCode: 200, data: obj });
};

exports.list_user = async function (req, res) {
  try {
    var p = res.connection.localPort;
    const limit = parseInt(req.query.limit) || 50;
    const page = parseInt(req.query.page) || 1;

    User.countDocuments({}, async function (err, count) {
      if (!err) {
        const users = await User.find()
          .lean()
          .sort({ name: 1 })
          .limit(limit)
          .skip(page - 1);

        for (let i = 0; i < users.length; i++) {
          if (users[i].pic) {
            users[i].pic.path =
              "http://localhost:" +
              p +
              "/storage/profile/" +
              users[i].pic.name +
              "";
          }
          delete users[i]._v;
          delete users[i].password;
          delete users[i].tokens;
        }

        return res.status(200).send({
          statusCode: 200,
          users,
          total: count,
          limit,
          page,
        });
      }
    });
  } catch (e) {
    res.status(500).send({ statusCode: 500, error: e.message });
  }
};
