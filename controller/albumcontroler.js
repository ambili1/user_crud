const Album = require("../models/Album");
const User = require("../models/User");
const Albumimages = require("../models/Albumimages");
var fs = require("fs");

exports.create_album = async function (req, res) {
  const album = new Album({
    ...req.body,
    owner: req.user._id,
  });

  try {
    await album.save();
    res.status(201).send({ statusCode: 201, data: album });
  } catch (e) {
    res.status(400).send({ statusCode: 400, error: e });
  }
};

exports.view_album = async function (req, res) {
  var img = await Albumimages.find({ owner: req.album._id })
    .lean()
    .select("image");

  var p = res.connection.localPort;

  for (let j = 0; j < img.length; j++) {
    if (img[j].image !== undefined) {
      img[j].path =
        "http://localhost:" + p + "/storage/album/" + img[j].image + "";
    }
  }

  req.album.images = img;
  res.status(200).send({
    statusCode: 200,
    crerated_by: req.user.name,
    album: req.album,
  });
};

exports.list_album = async function (req, res) {
  try {
    Album.countDocuments({}, async function (err, count) {
      if (!err) {
        var p = res.connection.localPort;
        let limit = parseInt(req.query.limit) || 50;
        let page = parseInt(req.query.page) || 1;

        let albums = await Album.find({ owner: req.user._id })
          .limit(limit)
          .skip(page - 1)

          .lean();
        for (let i = 0; i < albums.length; i++) {
          const album = albums[i];

          var images = await Albumimages.find({ owner: album._id })
            .select("image")
            .lean();

          for (let j = 0; j < images.length; j++) {
            var img = images[j];

            images[j].image =
              "http://localhost:" + p + "/storage/album/" + img.image + "";
          }
          let user = await User.findById(album.owner).select("name");
          album.createdBy = user;
          album.images = images;
          delete album.__v;
          delete album.owner;
        }
        return res.status(200).send({
          statusCode: 200,
          albums,
          total: albums.length,
          limit,
          page,
          count,
        });
      }
    });
  } catch (e) {
    res.status(500).send({ statusCode: 500, error: e.message });
  }

  /*
  try {
    const query = { owner: req.user._id };
    Album.countDocuments(query).exec((count_error, count) => {
      if (count_error) {
        return res.send(count_error);
      } else {
        Album.find(query)
          .skip(parseInt(req.query.skip) || 0)
          .limit(parseInt(req.query.limit) || 100)
          .populate([
            {
              path: "owner",
              select: "name",
              model: "User",
            },
          ])
          .exec(function (err, results) {
            return res
              .status(200)
              .send({
                satatusCode: 200,
                Albums: results,
                count: count,
                limit: parseInt(req.query.limit) || 100,
                page: parseInt(req.query.skip) + 1 || 1,
              });
          });
      }
    });
  } catch (e) {
    res.status(500).send({ statusCode: 500, error: e.message });
  }*/
};

exports.edit_album = async function (req, res) {
  const updates = Object.keys(req.body);
  const allowedUpdates = ["title", "description"];
  const isValidOperation = updates.every((update) =>
    allowedUpdates.includes(update)
  );

  if (!isValidOperation) {
    return res.status(400).send({ statusCode: 400, error: "Invalid updates!" });
  }

  try {
    const album = await Album.findOne({
      _id: req.params.id,
      owner: req.user._id,
    });

    if (!album) {
      return res
        .status(404)
        .send({ statusCode: 404, error: "Can not find This album for edit" });
    }

    updates.forEach((update) => (album[update] = req.body[update]));
    await album.save();
    res.status(200).send({ statusCode: 200, message: "Updated Succesfuly" });
  } catch (e) {
    res.status(400).send({ statusCode: 400, error: e });
  }
};

exports.add_albumimage = async function (req, res) {
  try {
    const albumimage = new Albumimages({
      image: req.file.filename,
      owner: req.album._id,
    });

    await albumimage.save();
    return res.status(201).send({ statusCode: 201, message: "image uploded" });
  } catch (e) {
    return res.status(400).send({ statusCode: 400, error: e });
  }
};

exports.delete_image = async function (req, res) {
  try {
    var img = await Albumimages.findOneAndDelete({
      owner: req.album._id,
      _id: req.params.img_id,
    });

    if (!img) {
      return res
        .status(404)
        .send({ statusCode: 404, message: "Cant Find image !!!!" });
    } else {
      var path = "./storage/album/" + img.image + "";

      fs.access(path, fs.F_OK, (err) => {
        if (!err) {
          fs.unlinkSync("./storage/album/" + img.image + "");
        }
      });

      res.status(200).send({ statusCode: 200, message: "image deleted" });
    }
  } catch (e) {
    return res.status(400).send({ statusCode: 400, error: e });
  }
};

exports.delete_album = async function (req, res) {
  try {
    var img = await Albumimages.find({ owner: req.album._id });

    for (let j = 0; j < img.length; j++) {
      console.log("./storage/album/" + img[j].image + "");

      fs.access("./storage/album/" + img[j].image + "", fs.F_OK, (err) => {
        if (!err) {
          fs.unlinkSync("./storage/album/" + img[j].image + "");
        }
      });

      await Albumimages.findOneAndDelete({ owner: req.album._id });
    }

    await Album.findOneAndDelete({ _id: req.album._id });
    res.status(200).send({ statusCode: 200, message: "album deleted" });
  } catch (e) {
    return res.status(400).send({ statusCode: 400, e });
  }
};
