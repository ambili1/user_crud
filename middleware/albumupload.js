const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "storage/album/");
  },
  filename: function (request, file, callback) {
    callback(null, Date.now() + file.originalname);
  },
});

const albumupload = multer({
  storage: storage,

  limits: {
    fileSize: 1000000,
  },
});

module.exports = albumupload;
