const Album = require("../models/Album");

const albumauth = async (req, res, next) => {
  try {
    const album = await Album.findOne({
      owner: req.user._id,
      _id: req.params.id,
    }).lean();

    if (!album) {
      throw new Error("No Such Album for this user");
    }

    req.album = album;
    next();
  } catch (e) {
    res
      .status(401)
      .send({ status: 401, error: "Unauthorized or Invalid album" });
  }
};

module.exports = albumauth;
