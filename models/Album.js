const mongoose = require("mongoose");

const albumSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    owner: 
      {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User",
      }
    
  },
  {
    timestamps: true,
  }
);
albumSchema.virtual("albumimages", {
  ref: "Albumimages",
  localField: "_id",
  foreignField: "owner",
});

const Album = mongoose.model("Album", albumSchema);

module.exports = Album;
