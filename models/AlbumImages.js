const mongoose = require('mongoose')

const albumimagesSchema = new mongoose.Schema({

   image: {
        type: String,
        required: true,
       
    },
    path: {
        type: String, 
       
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Album'
    }
}, {
    timestamps: true
})

const Albumimages = mongoose.model('Albumimages', albumimagesSchema)

module.exports = Albumimages;